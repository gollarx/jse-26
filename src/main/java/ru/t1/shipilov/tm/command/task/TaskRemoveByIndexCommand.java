package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-remove-by-index";

    @NotNull
    private final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() -1;
        getTaskService().removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
