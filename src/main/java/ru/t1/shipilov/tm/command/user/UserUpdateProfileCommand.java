package ru.t1.shipilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME");
        @NotNull final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
