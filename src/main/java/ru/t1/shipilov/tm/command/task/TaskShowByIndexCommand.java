package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.model.Task;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() -1;
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
